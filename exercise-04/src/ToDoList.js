import clsx from 'clsx'
import classes from './App.module.css'

export const ToDoList = (props) => {
    const { item, onToDoStatusChanged, onToDoRemove } = props

    if (!item || item.length === 0) {
        return (
            <div className={classes.toDoItemContainer}>
                <p>There are no to-do items!</p>
            </div>
        )
    }

    return (
        <>
            {item.map((item, idx) => {
                return (
                    <div
                        key={`todo-item-${idx}`}
                        className={clsx(
                            classes.toDoItemContainer,
                            item.isComplete && classes.todoItemComplete
                        )}
                    >
                        <label className={classes.todoItem}>
                            <input
                                id={`todo-input-${idx}`}
                                type="checkbox"
                                name={item.description}
                                checked={item.isComplete}
                                onChange={(e) =>
                                    onToDoStatusChanged(idx, e.target.checked)
                                }
                            />
                            <span>
                                {item.description}{' '}
                                {item.isComplete && '(Done!)'}
                            </span>
                        </label>
                        <button onClick={() => onToDoRemove(idx)}>
                            Remove
                        </button>
                    </div>
                )
            })}
        </>
    )
}
