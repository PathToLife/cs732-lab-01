import React, { useState } from 'react'
import { ToDoList } from './ToDoList'
import { ToDoAdd } from './ToDoAdd'

const initialToDos = [
    { description: 'Finish lecture', isComplete: true },
    { description: 'Do homework', isComplete: false },
    { description: 'Sleep', isComplete: true },
]

function App() {
    const [toDos, setToDos] = useState(initialToDos)

    const handleToDoStatusChanged = (idx, isChecked) => {
        const newToDos = [...toDos]
        newToDos[idx].isComplete = isChecked
        setToDos(newToDos)
    }

    const handleToDoRemove = (idx) => {
        const newToDos = [...toDos]
        newToDos.splice(idx, 1)
        setToDos(newToDos)
    }

    const handleAddToDo = (description) => {
        let newToDos = [...toDos]
        newToDos.push({
            description,
            isComplete: false,
        })
        setToDos(newToDos)
    }

    return (
        <div>
            <div>
                <h1>My todos</h1>
                <ToDoList
                    item={toDos}
                    onToDoStatusChanged={handleToDoStatusChanged}
                    onToDoRemove={handleToDoRemove}
                />
            </div>

            <div>
                <h1>Add item</h1>
                <ToDoAdd onAddToDo={handleAddToDo} />
            </div>
        </div>
    )
}

export default App
