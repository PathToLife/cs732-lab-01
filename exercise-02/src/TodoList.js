export const ToDoList = ({items}) => {

    if (items === undefined || !items.length) {
        return <p>There are no to-do items</p>
    }

    return (
        <ul>
            {items.map((item, idx) => {
                return (
                    <li key={`todo-item-${idx}`}>
                        {item}
                    </li>
                )
            })}
        </ul>
    )
}