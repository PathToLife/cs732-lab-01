import React, { useState } from 'react'
import classes from './App.module.css'

export const ToDoAdd = (props) => {
    const { onAddToDo } = props

    const [inputText, setInputText] = useState('')

    const handleSubmit = (e) => {
        e.preventDefault()
        if (inputText) {
            onAddToDo(inputText)
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <label className={classes.addTodo}>
                <span>Description:</span>
                <input
                    type="text"
                    value={inputText}
                    onChange={(e) => setInputText(e.target.value)}
                />
                <button type={'submit'}>Add</button>
            </label>
        </form>
    )
}
